<?php

namespace Drupal\favorite_things\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\favorite_things;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DefaultController.
 *
 * @package Drupal\favorite_things\Controller
 */
class DefaultController extends ControllerBase {

  /**
   * Favthings.
   *
   * @return string
   *   Return Hello string.
   */
  public function favThings() {
    drupal_flush_all_caches();
    $config = $this->config('favorite_things.FavConfig');
    $color = $config->get('fav_color');
    $favorite = $config->get('other_cat');
    $other = $config->get('other_fav');
    $food = $config->get('fav_food');

    if ($favorite != NULL && $other != NULL) {
      return [
        '#type' => 'markup',
        '#markup' => $this->t('Your favorite color is @color.<br>Your favorite food is @food.<br>Your favorite @favorite is @other.', array(
            '@color' => $color,
            '@food' => $food,
            '@favorite' => $favorite,
            '@other' => $other,
          )),
      ];
    } else {
      return [
        '#type' => 'markup',
        '#markup' => $this->t('Your favorite color is @color, and your favorite food is @food. You haven\'t picked any other favorites!', [
          '@color' => $color,
          '@food' => $food
          ]),
      ];
    }

  }


}
