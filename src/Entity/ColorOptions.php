<?php

namespace Drupal\favorite_things\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Color Options entity.
 *
 * @ConfigEntityType(
 *   id = "color_options",
 *   label = @Translation("Color Options"),
 *   handlers = {
 *     "list_builder" = "Drupal\favorite_things\ColorOptionsListBuilder",
 *     "form" = {
 *       "add" = "Drupal\favorite_things\Form\ColorOptionsForm",
 *       "edit" = "Drupal\favorite_things\Form\ColorOptionsForm",
 *       "delete" = "Drupal\favorite_things\Form\ColorOptionsDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\favorite_things\ColorOptionsHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "color_options",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/color_options/{color_options}",
 *     "add-form" = "/admin/structure/color_options/add",
 *     "edit-form" = "/admin/structure/color_options/{color_options}/edit",
 *     "delete-form" = "/admin/structure/color_options/{color_options}/delete",
 *     "collection" = "/admin/structure/color_options"
 *   }
 * )
 */
class ColorOptions extends ConfigEntityBase implements ColorOptionsInterface {

  /**
   * The Color Options ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Color Options label.
   *
   * @var string
   */
  protected $label;

}
