<?php

namespace Drupal\favorite_things\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Food entity entity.
 *
 * @ConfigEntityType(
 *   id = "food_entity",
 *   label = @Translation("Food entity"),
 *   handlers = {
 *     "list_builder" = "Drupal\favorite_things\FoodEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\favorite_things\Form\FoodEntityForm",
 *       "edit" = "Drupal\favorite_things\Form\FoodEntityForm",
 *       "delete" = "Drupal\favorite_things\Form\FoodEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\favorite_things\FoodEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "food_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/food_entity/{food_entity}",
 *     "add-form" = "/admin/structure/food_entity/add",
 *     "edit-form" = "/admin/structure/food_entity/{food_entity}/edit",
 *     "delete-form" = "/admin/structure/food_entity/{food_entity}/delete",
 *     "collection" = "/admin/structure/food_entity"
 *   }
 * )
 */
class FoodEntity extends ConfigEntityBase implements FoodEntityInterface {

  /**
   * The Food entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Food entity label.
   *
   * @var string
   */
  protected $label;

}
