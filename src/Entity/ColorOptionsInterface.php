<?php

namespace Drupal\favorite_things\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Color Options entities.
 */
interface ColorOptionsInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
