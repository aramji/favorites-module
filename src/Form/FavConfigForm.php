<?php

namespace Drupal\favorite_things\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Entity;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\taxonomy;

/**
 * Class FavConfigForm.
 *
 * @package Drupal\favorite_things\Form
 */
class FavConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityManager definition.
   *
   * @var \Drupal\Core\Entity\EntityManager
   */
  protected $entityManager;
  protected $entity;
  /**
   * Drupal\Core\Config\Entity\Query\QueryFactory definition.
   *
   * @var \Drupal\Core\Config\Entity\Query\QueryFactory
   */
  protected $entityQueryConfig;
  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityManager $entity_manager,
    QueryFactory $entity_query
    ) {
    parent::__construct($config_factory);
    $this->entityManager = $entity_manager;
    $this->entityQuery = $entity_query;
    $this->entity = $entity;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity.manager'),
      $container->get('entity.query')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'favorite_things.FavConfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fav_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('favorite_things.FavConfig');

    $query = $this->entityQuery->get('color_options');
    $colorlist = $query->execute();
    $colors = str_replace('_', ' ', $colorlist);

    $query2 = $this->entityQuery->get('food_entity');
    $foodlist = $query2->execute();
    $food = str_replace('_', ' ', $foodlist);

    // FAVORITE COLOR

    $form['fav_color'] = [
      '#type' => 'radios',
      '#title' => $this->t('Select a favorite color:'),
      '#description' => $this->t('Optional favorite colors'),
      '#options' => $colors,
      '#size' => 5,
      '#default_value' => $config->get('fav_color'),
    ];

    // FAVORITE FOOD

    $form['fav_food'] = [
      '#type' => 'radios',
      '#options' => $food,
      '#title' => $this->t('Might as well tell us your favorite food while you\'re here.'),
      '#description' => $this->t('Pick your favorite food!'),
      '#default_value' => $config->get('fav_food'),
    ];

    // PICK YOUR OWN FAVORITE

    $query3 = $this->entityQuery->get('taxonomy_vocabulary');
    $vocablist = $query3->execute();
    $vocabs = str_replace('_', ' ', $vocablist);
    $vocabs = array_merge(array($this->t('Select a topic')), $vocabs);

    $form['other_cat'] = [
      '#type' => 'select',
      '#options' => $vocabs,
      '#title' => $this->t('Pick a topic!'),
      '#description' => $this->t('To add a new topic and options, go to the Taxonomy page.'),
      '#default_value' => $config->get('other_cat'),
    ];

    if ($config->get('other_cat') != NULL) {
      $query4 = $this->entityQuery->get('taxonomy_term');
      $termlist = $query4->execute();

      // better way of doing this ??
      $terms = \Drupal\taxonomy\Entity\Term::loadMultiple($termlist);
      $length = count($terms)+1;
      $selected_vocab = $config->get('other_cat');

      for($i=0;$i<$length;$i++) {
        $name = $terms[$i];
        if($name != NULL) {
          // checking if terms are part of the selected vocab
          if ($selected_vocab == $name->getVocabularyId()) {
            // add corresponding terms to the options array
            $options[$name->getName()] = $name->getName();
          }
        }
      }

      $form['other_fav'] = [
        '#type' => 'radios',
        '#options' => $options,
        '#default_value' => $config->get('other_fav')
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('favorite_things.FavConfig')
      ->set('fav_color', $form_state->getValue('fav_color'))
      ->set('fav_food', $form_state->getValue('fav_food'))
      ->set('other_cat', $form_state->getValue('other_cat'))
      ->set('other_fav', $form_state->getValue('other_fav'))
      ->save();
  }

}
