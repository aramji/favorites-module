<?php

namespace Drupal\favorite_things\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FoodEntityForm.
 *
 * @package Drupal\favorite_things\Form
 */
class FoodEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $food_entity = $this->entity;
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $food_entity->label(),
      '#description' => $this->t("Label for the Food entity."),
      '#required' => TRUE,
    );

    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $food_entity->id(),
      '#machine_name' => array(
        'exists' => '\Drupal\favorite_things\Entity\FoodEntity::load',
      ),
      '#disabled' => !$food_entity->isNew(),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $food_entity = $this->entity;
    $status = $food_entity->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Food entity.', [
          '%label' => $food_entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Food entity.', [
          '%label' => $food_entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($food_entity->urlInfo('collection'));
  }

}
