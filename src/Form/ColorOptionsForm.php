<?php

namespace Drupal\favorite_things\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ColorOptionsForm.
 *
 * @package Drupal\favorite_things\Form
 */
class ColorOptionsForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $color_options = $this->entity;
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $color_options->label(),
      '#description' => $this->t("Label for the Color Options."),
      '#required' => TRUE,
    );

    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $color_options->id(),
      '#machine_name' => array(
        'exists' => '\Drupal\favorite_things\Entity\ColorOptions::load',
      ),
      '#disabled' => !$color_options->isNew(),
    );

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $color_options = $this->entity;
    $status = $color_options->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Color Options.', [
          '%label' => $color_options->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Color Options.', [
          '%label' => $color_options->label(),
        ]));
    }
    $form_state->setRedirectUrl($color_options->urlInfo('collection'));
  }

}
