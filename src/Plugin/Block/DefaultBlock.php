<?php

namespace Drupal\favorite_things\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'DefaultBlock' block.
 *
 * @Block(
 *  id = "default_block",
 *  admin_label = @Translation("Default block"),
 * )
 */
class DefaultBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Form\FormBuilder definition.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;
  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   */

  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        FormBuilder $form_builder
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = $this->formBuilder->getForm('Drupal\favorite_things\Form\FavConfigForm');
    $color = $form['fav_color']['#default_value'];
    $favorite = $form['other_cat']['#default_value'];
    $other = $form['other_fav']['#default_value'];
    $food = $form['fav_food']['#default_value'];

    $build = [];

    if ($favorite != NULL && $other != NULL) {
      $build['default_block']['#markup'] = $this->t('Color: @color.<br>@favorite: @other', [
        '@color' => $color,
        '@food' => $food,
        '@favorite' => $favorite,
        '@other' => $other,
      ]);
    } else {
      $build['default_block']['#markup'] = $this->t('Color: @color.', ['@color' => $color]);
    }

    return $build;
  }

}
